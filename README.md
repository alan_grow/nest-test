# NestJS and MongoDB backend sample

Boilerplate code...

## Features

Here I aim to use TypeScript features when possible.

## An API

### Testing/Using the API

Some options for trying out the different routes:

- Command line: HTTPie
- GUI: Insomnia REST Client
- Emacs: [restclient.el](https://github.com/pashky/restclient.el)

Example of test query using `restclient.el` (place the cursor over the line
with `POST` and do C-c C-c) to send the query; a response should appear in
the `*HTTP Response*` buffer:

```
POST http://localhost:3000/foo/blurb
Content-Type: application/json

{
	"title": "test blurb",
	"body": "this is a test",
	"upVotes": 1,
	"downVotes": 0
}
```

Example of `GET`ing a particular document:

```
GET http://localhost:3000/foo/blurb/5ddc514da747174794ed87f6
```

Example of `PUT`ing an update to a document:

```
PUT http://localhost:3000/foo/edit?blurbID=5ddc3cfaa747174794ed87f5
Content-Type: application/json

{
	"title": "I got edited",
	"body": "blarg!!",
	"upVotes": "3",
	"downVotes": "1"
}
```

Deleting a document is accomplished using a query parameter, just like
the `PUT` example above.

## Next steps

Boiler plate code I'll be adding features to as I learn including

- [ ] Authentication
- [ ] Routes to increment/decrement upvotes and downvotes
- [ ] Basic front end (consider Angular?)

## Resources

- [How To Build a Blog with Nest.js, MongogDB, and Vue.js](https://www.digitalocean.com/community/tutorials/how-to-build-a-blog-with-nest-js-mongodb-and-vue-js) (Feb. 2019, Oluyemi Olususi)
