export class CreateBlurbDTO {
    readonly title: string;
    readonly body: string;
    readonly upVotes: number;
    readonly downVotes: number;
}