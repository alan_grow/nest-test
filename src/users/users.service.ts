import { Injectable } from '@nestjs/common';

/* In a real app, this is where you'd build your user model and persistence 
 * layer, using your library of choice (e.g., TypeORM, Sequelize, Mongoose, 
 *  etc.).
 */

export type User = any;

@Injectable()
export class UsersService {
    private readonly users: User[];

    constructor() {
        this.users = [
            {
                userId: 1,
                username: 'root',
                password: 'password123',
            },
            {
                userId: 2,
                username: 'foo',
                password: 'bar'
            },
            {
                userId: 3,
                username: 'bar',
                password: 'baz'
            }
        ];
    }

    async findOne(username: string): Promise<User | undefined> {
        return this.users.find(user => user.username === username);
    }
}
