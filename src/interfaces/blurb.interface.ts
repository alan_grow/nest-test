import { Document } from 'mongoose';
export interface Blurb extends Document {
    readonly title: string;
    readonly body: string;
    readonly upVotes: number;
    readonly downVotes: number;
}
