import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Blurb } from '../interfaces/blurb.interface';
import { CreateBlurbDTO } from '../dto/create-blurb.dto';

@Injectable()
export class FooService {

    constructor(@InjectModel('Blurb') private readonly blurbModel: Model<Blurb>) { }

    async getBlurbs(): Promise<Blurb[]> {
        const blurbs = await this.blurbModel.find().exec();
        return blurbs;
    }
    async getBlurb(blurbID): Promise<Blurb> {
        const blurb = await this.blurbModel
            .findById(blurbID)
            .exec();
        return blurb;
    }

    async addBlurb(createBlurbDTO: CreateBlurbDTO): Promise<Blurb> {
        const newBlurb = await this.blurbModel(createBlurbDTO);
        return newBlurb.save();
    }

    async editBlurb(blurbID, createBlurbDTO: CreateBlurbDTO): Promise<Blurb> {
        const editedBlurb = await this.blurbModel
            .findByIdAndUpdate(blurbID, createBlurbDTO, { new: true });
        return editedBlurb;
    }

    async deleteBlurb(blurbID): Promise<any> {
        const deletedBlurb = await this.blurbModel
            .findByIdAndRemove(blurbID);
        return deletedBlurb;
    }
}
