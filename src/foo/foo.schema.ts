import * as mongoose from 'mongoose';

export const FooSchema = new mongoose.Schema({
    title: String,
    body: String,
    upVotes: Number,
    downVotes: Number,
});