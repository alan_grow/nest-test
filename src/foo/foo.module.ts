import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FooController } from './foo.controller';
import { FooService } from './foo.service';
import { FooSchema } from './foo.schema';

@Module({
    imports: [MongooseModule.forFeature([{
        name: 'Blurb',
        schema: FooSchema,
    }])],
    controllers: [FooController],
    providers: [FooService]
})
export class FooModule { }
