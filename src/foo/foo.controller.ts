import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Query, Put, Delete } from '@nestjs/common';
import { FooService } from './foo.service';
import { CreateBlurbDTO } from '../dto/create-blurb.dto';
import ValidateObjectId from './shared/pipes/validate-object-id.pipes';

@Controller('foo') // TODO think of better names for things 
export class FooController {
    constructor(private fooService: FooService) { }

    @Get('')
    async blurbsRoot(@Res() res)  {
        return res.status(HttpStatus.OK).json({
            "message": "Arrived at root of foo controller"
        })
    }

    @Get('blurbs')
    async getBlurbs(@Res() res) {
        const blurbs = await this.fooService.getBlurbs();
        return res.status(HttpStatus.OK).json(blurbs)
    }

    @Get('blurb/:blurbID')
    async getBlurb(@Res() res, @Param('blurbID', new ValidateObjectId()) blurbID) {
        const blurb = await this.fooService.getBlurb(blurbID);
        if (!blurb) {
            throw new NotFoundException('Blurb does not exist!');
        }
        return res.status(HttpStatus.OK).json(blurb);
    }

    @Post('/blurb')
    async addBlurb(@Res() res, @Body() createBlurbDTO: CreateBlurbDTO) {
        const newBlurb = await this.fooService.addBlurb(createBlurbDTO);
        return res.status(HttpStatus.OK).json({
            message: "Blurb has been submitted successfully!",
            blurb: newBlurb
        })
    }

    /* For now, upvote/downvote functionality can be accomplished using this 
       route */

    @Put('/edit')
    async editBlurb (
        @Res() res,
        @Query('blurbID', new ValidateObjectId()) blurbID,
        @Body() createBlurbDTO: CreateBlurbDTO
    ) {
        const editedBlurb = await this.fooService.editBlurb(blurbID, createBlurbDTO);
        if (!editedBlurb) {
            throw new NotFoundException('Blurb does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Blurb has been successfully updated',
            blurb: editedBlurb
        })
    }

    @Delete('/delete')
    async deleteBlurb(
        @Res() res,
        @Query('blurbID', new ValidateObjectId()) blurbID) {
            const deletedBlurb = await this.fooService.deleteBlurb(blurbID);
            if (!deletedBlurb) {
                throw new NotFoundException('Blurb does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Blurb has been deleted!',
                blurb: deletedBlurb,
            })
        }
}
